package com.ks.workerapp.rabbitmq;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.ErrorHandler;

@Configuration
public class RabitMQConfig {

    private static final String queue="ks.webapp.tasks";
    private static final String exchange="amq.direct";
    private static final String routingKey="bigtasks";
    
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Bean
    public Queue createQueue() {
        return new Queue(RabitMQConfig.queue);
    }

    @Bean 
    public TopicExchange exchange() {
        return new TopicExchange(RabitMQConfig.exchange);
    }
    
    
    @Bean
    public Binding binding(Queue queueArg, TopicExchange exchangeArg) {
        return BindingBuilder.bind(queueArg).to(exchangeArg).with(RabitMQConfig.routingKey);
    }

    @Bean
    public SimpleMessageListenerContainer container(ConnectionFactory connectionFactory, MessageListenerAdapter messageListenerAdapter) {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setQueueNames(queue);
        container.setErrorHandler(
            new ErrorHandler(){
            
                @Override
                public void handleError(Throwable arg0) {
                    logger.error(arg0.toString());
                }
            });
        messageListenerAdapter.setMessageConverter(converter());
        container.setMessageListener(messageListenerAdapter);
        
        return container;
    }

    @Bean
    public MessageListenerAdapter listenerAdapter(final TaskListener handler) {
        return new MessageListenerAdapter(handler, "receiveBigTaskMessage");
    }

    @Bean
    public Jackson2JsonMessageConverter converter() {
        return new Jackson2JsonMessageConverter();
    }
}