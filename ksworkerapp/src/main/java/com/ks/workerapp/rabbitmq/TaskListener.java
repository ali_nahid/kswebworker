package com.ks.workerapp.rabbitmq;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ks.workerapp.pojos.BigTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class TaskListener {
    
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    
    /*@RabbitListener(id="receiveBigTaskMessage", 
        bindings = @QueueBinding(
            value = @Queue(value = TaskListener.queue, durable = "true"),
            exchange =  @Exchange(value = TaskListener.exchange, durable = "true", delayed = "true"),
            key = TaskListener.routingKey        
        )
    )*/
    public void receiveBigTaskMessage(String message) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            BigTask task = mapper.readValue(message, BigTask.class);
            this.logger.info(task.toString());
        } catch(JsonProcessingException jpe) {

        }
        
    }
}