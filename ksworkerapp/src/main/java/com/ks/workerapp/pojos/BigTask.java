package com.ks.workerapp.pojos;

import java.io.Serializable;
import java.util.Map;
import java.util.UUID;
import java.util.Map.Entry;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class BigTask  {

    /**
     *
     */
    //private static final long serialVersionUID = 4695715020926478523L;

    @JsonProperty("taskId")
    private String taskId;

    @JsonProperty("tasksParams")
    private Map<String, String> tasksParams;

    /*public BigTask() {
        this.taskId = UUID.randomUUID().toString();
    }*/

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getTaskId() {
        return this.taskId;
    }

    public Map<String, String> getTasksParams() {
        return tasksParams;
    }

    public void setTasksParams(Map<String, String> tasksParams) {
        this.tasksParams = tasksParams;
    }    


    @Override
    public String toString() {
        String s= "taskId"+ this.taskId + " Task Params:";
        for(Entry<String,String> entry: this.tasksParams.entrySet()) {
            s += "["+entry.getKey()+":"+entry.getValue()+"] ";
        }

        return s;
    }
}