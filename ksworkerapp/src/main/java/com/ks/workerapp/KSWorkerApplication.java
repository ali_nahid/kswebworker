package com.ks.workerapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KSWorkerApplication {

	public static void main(String[] args) {
		SpringApplication.run(KSWorkerApplication.class, args);
	}

	
}
