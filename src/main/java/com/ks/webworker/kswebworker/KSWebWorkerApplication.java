package com.ks.webworker.kswebworker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KSWebWorkerApplication {

	public static void main(String[] args) {
		SpringApplication.run(KSWebWorkerApplication.class, args);
	}

}
