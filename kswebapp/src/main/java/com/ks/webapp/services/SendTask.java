package com.ks.webapp.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.ks.webapp.pojos.BigTask;

public interface SendTask {
    void send(BigTask bigTask) throws JsonProcessingException;
}