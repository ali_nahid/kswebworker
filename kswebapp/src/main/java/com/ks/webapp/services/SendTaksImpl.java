package com.ks.webapp.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ks.webapp.pojos.BigTask;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SendTaksImpl implements SendTask {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Override
    public void send(BigTask bigTask) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(bigTask);
        this.rabbitTemplate.convertAndSend(json);
    }
    
}