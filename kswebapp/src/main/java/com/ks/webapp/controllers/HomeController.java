package com.ks.webapp.controllers;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ks.webapp.pojos.BigTask;
import com.ks.webapp.services.SendTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/")
public class HomeController {
    

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private SendTask sendTask;

    @RequestMapping(method = RequestMethod.GET)
    public String index() {
        return "home";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String sendTaskSubmit(String taskPairs) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
            BigTask task = mapper.readValue(taskPairs, BigTask.class);
            this.sendTask.send(task);
        }catch(Exception e) {
            e.printStackTrace();;
            this.logger.error(e.toString());
        }
        

        return "redirect:/";
    }
}